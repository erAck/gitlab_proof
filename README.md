# Keyoxide OpenPGP Proof

[Verifying my OpenPGP key: openpgp4fpr:2265D7F3A7B095CC3918630B6A6CD5B765632D3A]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs
